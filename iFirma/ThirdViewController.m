//
//  ThirdViewController.m
//  iFirma
//
//  Created by Ali on 2013-07-20.
//  Copyright (c) 2013 Alnajar. All rights reserved.
//

#import "ThirdViewController.h"

@interface ThirdViewController ()

@end

@implementation ThirdViewController
@synthesize locationManager;
@synthesize geoCoder;
@synthesize address;
@synthesize longitude;
@synthesize texten;
@synthesize lat;

-(IBAction)textViewDidBeginEditing:(id)sender {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.35f];
    CGRect frame = self.view.frame;
    frame.origin.y = -200;
    [self.view setFrame:frame];
    [UIView commitAnimations];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    [txtView resignFirstResponder];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.35f];
    CGRect frame = self.view.frame;
    frame.origin.y = 20;
    [self.view setFrame:frame];
    [UIView commitAnimations];

    return NO;
}

-(IBAction)BackgroundThouched:(id)sender {
    [address resignFirstResponder];
}

-(IBAction)textFieldReturn:(id)sender {
    [sender resignFirstResponder];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.35f];
    CGRect frame = self.view.frame;
    frame.origin.y = 20;
    [self.view setFrame:frame];
    [UIView commitAnimations];
}

- (void)viewDidLoad
{
    texten.layer.contents = (id)[UIImage imageNamed:@"papperLines.png"].CGImage;
    texten.contentInset = UIEdgeInsetsMake(-4,0,-10,0);
    
    [longitude setEnabled: NO];
    [lat setEnabled: NO];
    [address setEnabled: NO];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
    
    lat.text  = [NSString stringWithFormat:@"%f", locationManager.location.coordinate.latitude];
    longitude.text = [NSString stringWithFormat:@"%f", locationManager.location.coordinate.longitude];
    
    [self.geoCoder reverseGeocodeLocation: locationManager.location completionHandler:
     
     ^(NSArray *placemarks, NSError *error) {
         
         //Get nearby address
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         
         //String to hold address
         NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
    
         //Print the location to console
         NSLog(@"I am currently at %@",locatedAt);
         
         //Set the label text to current location
         address.text = locatedAt;
     }];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSInteger textLength = 0;
    textLength = [textField.text length] + [string length] - range.length;
    NSLog(@"Length: %d", textLength);
    return YES;
}

-(IBAction)save:(id)sender{
    //NSString *strURL = [NSString stringWithFormat:@"?];
    //NSData *dataURL = [NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]];
    //Example form with one php variable only. Use get URL argument notation to add more args.
    
    NSString *rawStr = [NSString stringWithFormat:@"?location=%@&texten=%@&long=%@&lat=%@",address.text,texten.text,longitude.text,lat.text];
    
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:@"http://alioriyadho.info/ifirma/phpFile.php"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
    
    NSURLResponse *response;
    NSError *err;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    NSLog(@"responseData: %@", responseData);
    NSLog(@"responseData: %@", rawStr);
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancel:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  ThirdViewController.h
//  iFirma
//
//  Created by Ali on 2013-07-20.
//  Copyright (c) 2013 Alnajar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>

@interface ThirdViewController : UIViewController
{
    IBOutlet CLLocationManager *locationManager;
}

-(IBAction)cancel:(id)sender;
-(IBAction)BackgroundThouched:(id)sender;
-(IBAction)textFieldReturn:(id)sender;
-(IBAction)textViewDidBeginEditing:(id)sender;
-(IBAction)save:(id)sender;

@property (nonatomic, retain) IBOutlet UITextField *address;
@property (nonatomic, retain) IBOutlet UITextField *longitude;
@property (nonatomic, retain) IBOutlet UITextView *texten;
@property (nonatomic, retain) IBOutlet UITextField *lat;
@property (strong, nonatomic) IBOutlet CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet CLGeocoder *geoCoder;
@end

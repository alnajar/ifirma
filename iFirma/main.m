//
//  main.m
//  iFirma
//
//  Created by Ali on 2013-07-20.
//  Copyright (c) 2013 Alnajar. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
